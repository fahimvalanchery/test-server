const express = require("express");
const app = express();
const cors = require("cors");
const config = require("config");
const PORT =config.get('port')
const expressip = require("express-ip");




// Cors setting
app.use(cors());
app.use(express.json({ limit: "100mb" }));
app.use(express.urlencoded({ extended: true }));
app.use(expressip().getIpInfoMiddleware);

// user related routes
app.use('/api/user',require('./src/routers/user.router'))
// dashboard related routes
app.use('/api/dashboard',require('./src/routers/dashboard.router'))


app.listen(PORT,()=>{
    console.log(`Server Running on PORT: ${PORT}`)
})