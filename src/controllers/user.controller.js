const geoip = require('geoip-lite');
const myip = require("ip");



/**
 * Fetching the User ip
 * @param {*} req 
 * @param {*} res 
 * Return Ip related data
 */
const getUserIP = async (req, res) => {
    try {
        let { ip } = req
        // if it is using as local
        // it will take system ip
        if (ip == "::1") {
            ip = myip.address()
        }

        const geo = geoip.lookup(ip);
        res.send(geo)

    } catch (error) {
        console.log(error);
        res.status(500).send(error)
    }
}


module.exports = {
    getUserIP
}