const { axiosGet } = require("../helper/axios")
const config = require('config')
const crypto_api = config.get('crypto_api')

/**
 * Fetching the crypto currency data
 * @param {*} req 
 * @param {*} res 
 * Return graph data
 */
const getCryptoCurrencyExchangeRates = async (req, res) => {
    try {
        let apiData = await axiosGet(crypto_api)
        let rates = {}
        if (apiData && apiData.rates) {
            rates = apiData.rates
        }
        let data = []
        let labels = []
        let result = {
            labels: [],
            datasets: []
        }
        for (let key in rates) {
            if (rates[key]['type'] == "crypto") {
                if (rates[key]['value'] < 1000) {
                    labels.push(rates[key]['name'])
                    data.push(rates[key]['value'])
                }
            }
        }


        result.labels = labels
        result.datasets.push({ data, label: "Crypto vs Bitcoin" })
        res.send(result)
    } catch (error) {
        res.status(500).send(error)
    }
}
/**
 * Fetching the crypto currency data
 * @param {*} req 
 * @param {*} res 
 * Return graph data
 */
const getNonCryptoCurrencyExchangeRates = async (req, res) => {
    try {
        let apiData = await axiosGet(crypto_api)
        let rates = {}
        if (apiData && apiData.rates) {
            rates = apiData.rates
        }
        let data = []
        let labels = []
        let result = {
            labels: [],
            datasets: []
        }
        for (let key in rates) {
            if (rates[key]['type'] !== "crypto") {
                if (rates[key]['value'] < 100000) {
                    labels.push(rates[key]['name'])
                    data.push(rates[key]['value'])
                }
            }
        }


        result.labels = labels
        result.datasets.push({ data, label: "Non Crypto vs Bitcoin" })
        res.send(result)
    } catch (error) {
        res.status(500).send(error)
    }
}


module.exports = {
    getCryptoCurrencyExchangeRates,
    getNonCryptoCurrencyExchangeRates
}