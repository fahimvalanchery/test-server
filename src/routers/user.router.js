const express = require("express");
const router = express.Router();
const UserController = require('../controllers/user.controller')

// route to fetch user data by ip
router.get('/ip', UserController.getUserIP)



module.exports = router;