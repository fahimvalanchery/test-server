const express = require("express");
const router = express.Router();
const DashboardController =require('../controllers/dashboard.controller')

// route to fetchcrypto graph
router.get('/graph/crypto', DashboardController.getCryptoCurrencyExchangeRates)
router.get('/graph/noncrypto', DashboardController.getNonCryptoCurrencyExchangeRates)


module.exports = router;