const axios = require('axios')

const axiosGet = async (url, options = {}) => {
   return await axios.get(url, options).then(res=>res.data).catch(err=>{throw err})
}


module.exports = {
    axiosGet
}